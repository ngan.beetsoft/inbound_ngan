<header class="header">
	<div class="inner">
		<div class="logo">
			<a href="/2020">2020 現場対応者向け<br>オンライン講話</a>
		</div>
		<nav class="menu">
			<ul>
				<li><a href="/2020">トップ</a></li>
				<li><a href="/2020/index.php#kicho">基調講話</a></li>
				<li><a href="/2020/index.php#bunya">分野別講話</a></li>
				<li><a href="/2020/index.php#kokunai">国内事例</a></li>
			</ul>
		</nav>
		<button type="button" class="btn_menu" onclick="toggleNav(this)">
			<span class="bar bar1"></span>
			<span class="bar bar2"></span>
			<span class="bar bar3"></span>
		</button>
	</div>
</header>
