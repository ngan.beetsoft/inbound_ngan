<?php
//時限設定のための初期設定
// UNIX TIMESTAMPを取得
date_default_timezone_set('Asia/Tokyo');
$timestamp = time() ;
//現在の日時（例）202011190000
$nowtime = date( "YmdHi" , $timestamp ) ;

// $nowtime = 202011190000 ;

 ?>


	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- 最新のIEで表示する -->
	<meta name="viewport" content="width=device-width, user-scalable=0">
	<meta name="format-detection" content="telephone=no"><!-- 電話番号の自動リンクを無効化 -->

	<link rel="stylesheet" href="/common/css/style.css">
	<link rel="stylesheet" href="/common/css/style_sp.css">
	<script src="/common/js/jquery.js"></script>
	<link rel="icon" href="/common/images/favicon.ico">
