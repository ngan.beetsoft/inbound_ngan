<?php
// zipファイル名
$fileName = "formSample.zip";

// 圧縮対象フォルダ
$compressDir = __DIR__;

// zipファイル保存先
$zipFileSavePath = sys_get_temp_dir()."/";

// コマンド
// cd：ディレクトリの移動
// zip:zipファイルの作成
$command =  "cd ". $compressDir .";"."zip -r ".$zipFileSavePath.$fileName." .";

// Linuxコマンドの実行
exec($command);

// 圧縮したファイルをダウンロードさせる。
header('Pragma: public');
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$fileName);
readfile($zipFileSavePath.$fileName);

?>
