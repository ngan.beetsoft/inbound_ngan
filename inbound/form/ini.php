<?php
mb_language('japanese');
mb_internal_encoding('UTF-8');
ini_set('session.cookie_secure', '1');

//後で変更となる可能性のある初期値をまとめる

$viewDir = __DIR__."/view/";
$modelDir = __DIR__."/model/";

// $adminMail = 'dstaff@imagesource.jp';//管理者
$adminMail = 'info@inbound-seminar.jp';//管理者
$from = 'info@inbound-seminar.jp';//差出人

$subjectAdmin = "【現場対応者向けオンライン講話サイト】動画への質問がありました。";
$subject = "【現場対応者向けオンライン講話サイト】動画への質問を送信いたしました。";

//外部から受け取ったデータは全てsanitizeする
//XSS、SqlInjection対策
include_once($modelDir.'sanitize.php');
$sanitizeC = new sanitizeClass();
$_POST = $sanitizeC->mb_convert_change($_POST, "KVa", "UTF-8");
$_GET = $sanitizeC->mb_convert_change($_GET, "KVa", "UTF-8");
$_POST = $sanitizeC->sanitizeFunc($_POST);
$_GET = $sanitizeC->sanitizeFunc($_GET);
//クリックジャッキング対策
include_once($modelDir.'view.php');
$viewC = new viewClass();
//CSRF対策
include_once($modelDir.'csrf.php');
$csrfC = new csrfClass();
?>
