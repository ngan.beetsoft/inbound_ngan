﻿<?php
class csrfClass {
// token生成
  function getCSRFToken(){
      $token = substr(bin2hex(random_bytes(32)), 0, 32);;
      setcookie('XSRF-TOKEN', $token);
      return $token;
  }
//tokenチェック
  function validateCSRFToken($token){
    if(!preg_match("/$token/",$_COOKIE['XSRF-TOKEN'])):
      // $loc = 'Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $loc = 'Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      header($loc);
      exit;
    endif;
  }
}
?>
