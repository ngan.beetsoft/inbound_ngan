<?php

class sanitizeClass {

  function sanitizeFunc($array){
      $patterns = array("/</", "/\//", "/>/", "/'/",'/\"/','/;/');
      $replacements = array("＜", "／", "＞", "’",'”','；');
      if (is_array($array)){
          foreach($array as $i => $key){
              if(is_array($key)){
                  $array[$i] = preg_replace($patterns, $replacements, $array[$i]);
              }else{
                  $array[$i] = preg_replace($patterns, $replacements, $key);
              }
          }
      }else{
            $array = preg_replace($patterns, $replacements, $array);
      }
      return $array;
  }

  function mb_convert_change($array, $option, $encoding){
      if (is_array($array)){
          foreach($array as $i => $key){
              if(is_array($key)){
                  $array[$i] = mb_convert_change($array[$i], $option, $encoding);
              }else{
                  $array[$i] = mb_convert_kana($key, $option, $encoding);
              }
          }
      }else{
          $array = mb_convert_kana($array, $option, $encoding);
      }
      return $array;
  }

/*
  function refererChk($domain, $selfPath){
    if(!preg_match("/$domain/",$_SERVER['HTTP_REFERER'])):
      $loc = 'Location: https://'.$domain.$selfPath;
      header($loc);
      exit;
    endif;
  }
*/
}
?>
