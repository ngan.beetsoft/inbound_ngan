<?php

class validationClass {

//複数のバリデーション項目がある場合は、以下のサンプルのように複数チェックし
//受け取る変数を .= で追加してください。。
/*
$errMsg['name'] = $validC->vRequired('name');
$errMsg['name'] .= $validC->vStrlenHiragana('name');
$errMsg['name'] .= $validC->vStrlenMinMax('name', 1, 5);
*/

//必須項目
  function vRequired($key){
    if (!$_POST[$key]) {
     return '必須項目です。';
    }
  }

//メールアドレスのみ許可
  function vMail($key){
    if (!preg_match('|^[0-9a-z_./?-]+@([0-9a-z-]+\.)+[0-9a-z-]+$|', $_POST[$key])) {
    return  'メールアドレスが不正です。';
    }
  }

//半角数字のみ許可
  function vNumlic($key){
    if (!preg_match("/^[0-9]+$/", $_POST[$key])) {
     return '半角数字で入力してください。';
    }
  }

//半角英数字のみ許可
  function vAlphaNumlic($key){
    if (!preg_match("/^[a-zA-Z0-9]+$/", $_POST[$key])) {
     return '半角英数字で入力してください。';
    }
  }


//郵便番号のみ許可
  function vPostcode($key){
    if (!preg_match("/^[0-9]{7}$/", $_POST[$key])) {
     return '半角数字７桁で入力してください。';
    }
  }

//電話番号として妥当なもののみ許可
  function vTel($key){
    if (
      !preg_match("/\A0(\d{1}[-(]?\d{4}|\d{2}[-(]?\d{3}|\d{3}[-(]?\d{2}|\d{4}[-(]?\d{1})[-)]?\d{4}\z/", $_POST[$key]) //固定電話
      &&
      !preg_match("//\A0[5789]0[-(]?\d{4}[-)]?\d{4}\z/", $_POST[$key])//携帯電話
    ) {
     return '正しい番号を入力してください。';
    }
  }

//〇〇文字以下
  function vStrlenMax($key,$max){
    if (!(mb_strlen($_POST[$key], "UTF-8") <= $max )) {
     return $max.'文字以下で入力してください。';
    }
  }

  //〇〇文字以上
    function vStrlenMin($key,$min){
      if (!(mb_strlen($_POST[$key], "UTF-8") >= $min )) {
       return $min.'文字以上で入力してください。';
      }
    }



//〇〇文字以上〇〇文字以下
  function vStrlenMinMax($key,$min,$max){
    if (!($min <= mb_strlen($_POST[$key], "UTF-8") && mb_strlen($_POST[$key], "UTF-8") <= $max )) {
     return $min.'文字以上、'.$max.'文字以下で入力してください。';
    }
  }

//全てひらがな
  function vStrlenHiragana($key){
    if(preg_match('/[^ぁ-んー$]/u',$_POST[$key])){
     return '「ひらがな」で入力してください。';
    }
  }

//全てカタカナ
  function vStrlenKatakana($key){
    if(preg_match('/[^ァ-ヶー$]/u',$_POST[$key])){
     return '「カタカナ」で入力してください。';
    }
  }



}
?>
