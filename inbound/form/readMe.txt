/////////////////////////////////
ファイルの構成
/////////////////////////////////

ini.php　　　　　　　←←←←初期設定値が定義されているファイル
index.php       　　←←←←contoroler
model (folder)　　　←←←←オリジナル関数が入っているフォルダ
  csrf.php　　　　　←←←←CSRF対策用の関数
  sanitize.php　　　←←←←sanitize用の関数
  sendmail.php　　　←←←←sendmail用の関数
  validation.php　　←←←←validation用の関数
  veiw.php　　　　　←←←←view用の関数（クリックジャッキング対策用）
view (folder)　　　　←←←←フロントエンドのHTMLが入っているフォルダ
  complete.php　　　←←←←完了画面のHTML
  confirm.php　　　←←←←確認画面のHTML
  footer.php　　　　←←←←フッター部分のHTML
  header.php　　　　←←←←ヘッダー部分のHTML
  index.php　 　　　←←←←入力画面のHTML
download.php　　　　←←←←最新のformSampleディレクトリ全ファイルをダウンロードする用


/////////////////////////////////
何故このようにしたいのか
/////////////////////////////////


セキュリティ的な部分
　外部より受け取ったデータは全てsanitizeしたい
　XSS、SqlInjeciton、CSRF、clickJacking対策済み

Model View Contoroler　を切り分けたい
　フロントのHTMLコードと
　サーバーサイドのプログラムの分離

validationは
　セキュリティ的にサーバーサイドでやりたい。

日本語の取り扱いが難しいので
　こちらで用意したvalidationを使っていただくとスムーズ
　javascriptのフロントエンドのvalidationもあれば良いが、なくても良い。
　JavaScriptでのValidationやPostはトラブルが多いので極力避けたい。
