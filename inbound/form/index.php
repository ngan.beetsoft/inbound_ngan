<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>質問フォーム | 2021 現場対応者向けオンライン講話サイト</title>
</head>
<body>
	<?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/header_2021.php");
    ?>
<?php
include_once('ini.php');
session_start(); //session開始
if (isset($_POST['controler'])) :
    if ($_POST['controler'] == 'confirm') :
        //確認画面
        $csrfC->validateCSRFToken($_POST['token']); //token確認　処理の前に必須

        //validation start
        include_once($modelDir . 'validation.php');
        $validC = new validationClass();

        //validation項目 start

		//お名前
		if(!$_POST['name']){
			$errMsg['name'] = $validC->vRequired('name');
		}else{
			$errMsg['name'] = $validC->vStrlenMinMax('name', 1, 50);
		}

		//所属
		if(!$_POST['belonging']){
			$errMsg['belonging'] = $validC->vRequired('belonging');
		}else{
			$errMsg['belonging'] = $validC->vStrlenMinMax('belonging', 1, 100);
		}

		//メールアドレス
		if(!$_POST['email']){
        	$errMsg['email']  = $validC->vRequired('email');
		}else{
			$errMsg['email']  = $validC->vStrlenMax('email', 200);
	        $errMsg['email'] .= $validC->vMail('email');
		}


		//質問事項
		if(!$_POST['question_matters']){
        	$errMsg['question_matters']  = $validC->vRequired('question_matters');
		}else{
			$errMsg['question_matters']  = $validC->vStrlenMax('question_matters', 1000);
		}


        //validation項目 end

        foreach ($errMsg as $key => $val) :
            if (!empty($errMsg[$key])) : $inputErr = true;
            endif;
        endforeach;

        //エラーがあればindexへ戻す
        if (isset($inputErr)) : $_POST['controler'] = 'index';
        endif;

        //validation end

        //入力値をsessionに記録
        foreach ($_POST as $key => $val) : $_SESSION[$key] = $val;
        endforeach;


    elseif ($_POST['controler'] == 'complete') :
        if(isset($_SESSION['name'])):
            //完了画面
            $csrfC->validateCSRFToken($_POST['token']); //token確認　処理の前に必須
            include_once($modelDir . 'sendmail.php');


            $msendC = new mbSendmailClass();
            $headers = "From: $from\nReply-To: $from\n";

            //必要であればメール送る
$messageAdmin = <<<EOF
以下の内容で質問を受け付けいたしました。
担当者は確認をお願いいたします。

━━━━━━□■□　質問内容　□■□━━━━━━
【お名前】$_SESSION[name]
【所属】$_SESSION[belonging]
【メールアドレス】$_SESSION[email]
【動画名】$_SESSION[video_name]
【質問事項】
$_SESSION[question_matters]

━━━━━━━━━━━━━━━━━━━━━━━━━━━━
EOF;

$message = <<<EOF
※このメールはシステムからの自動返信です

$_SESSION[name]様

以下の内容で質問を受け付けいたしました。
内容を確認後、担当者よりご連絡いたしますので
今しばらくお待ちくださいませ。

━━━━━━□■□　質問内容　□■□━━━━━━
【お名前】$_SESSION[name]
【所属】$_SESSION[belonging]
【メールアドレス】$_SESSION[email]
【動画名】$_SESSION[video_name]
【質問事項】
$_SESSION[question_matters]

━━━━━━━━━━━━━━━━━━━━━━━━━━━━

━━━━━━━━━━━━━━━━━━━━━━━━━━━━

2020 現場対応者向けオンライン講話 事務局
URL：https://inbound-seminar.jp/
メール：info@inbound-seminar.jp

━━━━━━━━━━━━━━━━━━━━━━━━━━━━

EOF;
            //必要であればメール送る
            $msendC->sendMail($adminMail, $subjectAdmin, $messageAdmin, $headers,'-f'.$from);//メール管理者
            $msendC->sendMail($_SESSION['email'], $subject, $message, $headers,'-f'.$from);//メール送信

            //sessin破棄
            session_destroy();
        endif;

    endif;
else :
    //マッチしない場合は
    if (!isset($_SESSION['video_name']) && !isset($_POST['video_name'])) :
        header('Location: /');
    endif;
    $_POST['controler'] = 'index';
endif;

//view読み込み
$viewC->clickjackingMeasures(); // クリックジャッキング対策　HTMLを書き出す前に読み込み必須
$token = $csrfC->getCSRFToken(); // CSRF対策　HTMLを書き出す前に読み込み必須
include($_SERVER['DOCUMENT_ROOT'] . "/navi/header_2021.php");
include_once($viewDir . $_POST['controler'] . '.php');
