<?php
include_once('./ini.php');
?>
<main class="page_contact">
  <section class="banner">
    <h1 class="ttl_2">ご質問・ご感想フォーム</h1>
    <img src="/common/images/banner.png" class="pc" alt="基調講話">
    <img src="/common/images/banner_sp.png" class="sp" alt="基調講話">
  </section>
  <nav class="c_breadcrumb">
    <div class="inner">
      <ol class="c_breadcrumb_list">
        <li class="c_breadcrumb_item"><a href="/">TOP</a> ＞ </li>
        <li class="c_breadcrumb_item">ご質問・ご感想フォーム</li>
      </ol>
    </div>
  </nav>
  <section class="content">
    <div class="inner">
      <div class="ttl bg-5">
        <h2 class="ttl_3">ご質問・ご感想フォーム</h2>
      </div>
      <p class="ttl_4 mb-3 text-center w-100">以下の内容で間違いなければ、「送信」ボタンを押してください。</p>
      <div class="box_form">

        <form action="./" class="form-contact" method="post" id="form-confirm">
          <input type="hidden" name="controler" value="complete">
          <input type="hidden" name="token" value="<?= $token ?>">
          <div class="form-contact-body">
            <div class="form-contact-row row">
              <div class="left">
                <div class="label-group">
                  お名前
                </div>
              </div>
              <div class="right">
                <div class="field-group">
                  <?php if (isset($_SESSION['name'])) {echo $_SESSION['name'];}; ?>
                </div>
              </div>
            </div>
            <div class="form-contact-row row">
              <div class="left">
                <div class="label-group">
                  所属
                </div>
              </div>
              <div class="right">
                <div class="field-group">
                  <?php if (isset($_SESSION['belonging'])) {echo $_SESSION['belonging'];}; ?>
                </div>
              </div>
            </div>
            <div class="form-contact-row row">
              <div class="left">
                <div class="label-group">
                  メールアドレス
                </div>
              </div>
              <div class="right">
                <div class="field-group">
                  <?php if (isset($_SESSION['email'])) {echo $_SESSION['email'];}; ?>
                </div>
              </div>
            </div>
            <div class="form-contact-row row">
              <div class="left">
                <div class="label-group">
                  動画名
                </div>
              </div>
              <div class="right">
                <div class="field-group">
                  <?php if (isset($_SESSION['video_name'])) {echo $_SESSION['video_name'];}; ?>
                </div>
              </div>
            </div>
            <div class="form-contact-row row bd_none">
              <div class="left">
                <div class="label-group">
                  質問事項
                </div>
              </div>
              <div class="right">
                <div class="field-group">
                  <?php if (isset($_SESSION['question_matters'])) {echo nl2br ($_SESSION['question_matters']);}; ?>
                </div>
              </div>
            </div>
            <div class="group-btn group-btn-confirm">

              <button type="submit" name="controler" value="index" class="btn btn-apply btn-back mr-1">戻る</button>

              <button type="submit" name="controler" value="complete" class="btn btn-apply btn_1" id="send_mail">送信</button>

            </div>
        </form>
      </div>
    </div>
  </section>
</main>

<div class="loader">
  <div class="spin"></div>
</div>

<?php include_once($viewDir . 'footer.php'); ?>