<?php
include_once('./ini.php');
include_once($viewDir . 'header.php');
?>

<main class="page_contact">
  <section class="banner">
    <h1 class="ttl_2">ご質問・ご感想フォーム</h1>
    <img src="/common/images/banner.png" class="pc" alt="基調講話">
    <img src="/common/images/banner_sp.png" class="sp" alt="基調講話">
  </section>
  <nav class="c_breadcrumb">
    <div class="inner">
      <ol class="c_breadcrumb_list">
        <li class="c_breadcrumb_item"><a href="/">TOP</a> ＞ </li>
        <li class="c_breadcrumb_item">ご質問・ご感想フォーム</li>
      </ol>
    </div>
  </nav>
  <section class="content">
    <div class="inner">
      <div class="ttl bg-5">
        <h2 class="ttl_3">ご質問・ご感想フォーム</h2>
      </div>
      <p class="ttl_4 mb-3 text-center w-100">送信完了いたしました。<br>
        内容を確認の上、担当者より後日返信いたしますので今しばらくお待ちください。</p>
    </div>
  </section>
</main>

<?php include_once($viewDir . 'footer.php'); ?>