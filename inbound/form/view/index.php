<?php
include_once('./ini.php');
?>
<main class="page_contact">
  <section class="banner">
    <h1 class="ttl_2"> ご質問・ご感想フォーム</h1>
    <img src="/common/images/banner.png" class="pc" alt="基調講話">
    <img src="/common/images/banner_sp.png" class="sp" alt="基調講話">
  </section>
  <nav class="c_breadcrumb">
    <div class="inner">
      <ol class="c_breadcrumb_list">
        <li class="c_breadcrumb_item"><a href="/2021">TOP</a> ＞ </li>
        <li class="c_breadcrumb_item"> ご質問・ご感想フォーム</li>
      </ol>
    </div>
  </nav>
  <section class="content">
    <div class="inner">
    <p class="txt">動画についてのご質問・ご感想があればぜひ以下のフォームよりお送りください。<br />
いただいた内容は講師に伝えさせていただきます。また、ご質問の場合は、後日ご返信させていただきます。</p>
    <div class="ttl bg-5">
        <h2 class="ttl_3"> ご質問・ご感想フォーム</h2>
    </div>
    <div class="box_form">
      <form action="./" class="form-contact" method="post" id="register_frm">
        <input type="hidden" name="controler" value="confirm">
        <input type="hidden" name="token" value="<?= $token ?>">
        <input type="hidden" name="title_name" value="<?php echo $_POST['title_name'];?>">
        <div class="form-contact-row row">
          <div class="left">
            <div class="label-group">
              お名前 <span class="required">（必須）</span>
            </div>
          </div>
          <div class="right">
            <div class="field-group">
              <input type="text" class="form-control" placeholder="例）インバウンド　太郎" name="name" maxlength="50" value="<?php if (isset($_SESSION['name'])) {echo $_SESSION['name'];}; ?>">
              <?php if (isset($errMsg['name'])) {
                echo '<label class="error">'.$errMsg['name']."</label>";
              }; ?>
            </div>
          </div>
        </div>
        <div class="form-contact-row row">
          <div class="left">
            <div class="label-group">
              所属 <span class="required">（必須）</span>
            </div>
          </div>
          <div class="right">
            <div class="field-group">
              <input type="text" class="form-control" placeholder="例）〇〇町観光協会 等" name="belonging" maxlength="100" value="<?php if (isset($_SESSION['belonging'])) {echo $_SESSION['belonging'];}; ?>">
              <?php if (isset($errMsg['belonging'])) {
                echo '<label class="error">'.$errMsg['belonging']."</label>";
              }; ?>
            </div>
          </div>
        </div>
        <div class="form-contact-row row">
          <div class="left">
            <div class="label-group">
              メールアドレス <span class="required">（必須）</span>
            </div>
          </div>
          <div class="right">
            <div class="field-group">
              <input type="text" class="form-control" placeholder="例）aaaaaa@inbound-seminar.jp" name="email" maxlength="200" value="<?php if (isset($_SESSION['email'])) {echo $_SESSION['email'];}; ?>">
              <?php if (isset($errMsg['email'])) {
                echo '<label class="error">'.$errMsg['email']."</label>";
              }; ?>
            </div>
          </div>
        </div>
        <div class="form-contact-row row">
          <div class="left">
            <div class="label-group">
              動画名 <span class="required">（必須）</span>
            </div>
          </div>
          <div class="right">
            <div class="field-group">
              <p>
                <?php if (isset($_SESSION['video_name'])) {echo $_SESSION['video_name'];}else {if (isset($_POST['video_name'])) {echo $_POST['video_name'];}}; ?>
              </p>
              <input type="hidden" class="form-control" placeholder="aaa@bbb.com" name="video_name" value="<?php if (isset($_SESSION['video_name'])) {echo $_SESSION['video_name'];}else {if (isset($_POST['video_name'])) {echo $_POST['video_name'];}}; ?>">
              <input type="hidden" name="title_name" value="<?php if (isset($_SESSION['title_name'])) {echo $_SESSION['title_name'];}else {if (isset($_POST['title_name'])) {echo $_POST['title_name'];}}; ?>">
              <?php if (isset($errMsg['video_name'])) {
                echo '<label class="error">'.$errMsg['video_name']."</label>";
              }; ?>
            </div>
          </div>
        </div>
        <div class="form-contact-row bd_none row">
          <div class="left">
            <div class="label-group">
            ご質問・ご感想 <span class="required">（必須）</span>
            </div>
          </div>
          <div class="right">
            <div class="field-group">
              <textarea name="question_matters" id="" class="form-control" cols="30" rows="10" maxlength="1000"><?php if (isset($_SESSION['question_matters'])) {echo $_SESSION['question_matters'];}; ?></textarea>
              <?php if (isset($errMsg['question_matters'])) {
                echo '<label class="error">'.$errMsg['question_matters']."</label>";
              }; ?>
            </div>
          </div>
        </div>
        <div class="group-btn">
          <button type="submit" name="controler" value="confirm" class="btn btn_1 btn-apply">確認画面へ進む</button>
        </div>
      </form>
      </div>
    </div>
  </section>
</main>
<?php include_once($viewDir . 'footer.php'); ?>
