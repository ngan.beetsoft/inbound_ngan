<?php

$cat            = "分野別講話";//カテゴリー
$title          = "SNSによる外国人向け自治体情報発信［実践編］";//動画タイトル
$teacherName    = "小森 彩花";//講師
 ?>

<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title><?php echo $title; ?> | <?php echo $cat; ?> | 2021 現場対応者向けオンライン講話サイト</title>
</head>



<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/header_2021.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2"><?php echo $cat; ?><br />［SNS］</h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
            <div class="inner">
                <ol class="c_breadcrumb_list">
                    <li class="c_breadcrumb_item"><a href="/2021">TOP</a> ＞ </li>
                    <li class="c_breadcrumb_item"><a href="/2021/index.php#bunya"><?php echo $cat; ?></a> ＞</li>
                    <li class="c_breadcrumb_item">『<?php echo $title; ?>』<?php echo $teacherName; ?></li>
                </ol>
            </div>
        </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3"><?php echo $title; ?></h2>
                        <input type="hidden" name="video_name" value="『<?php echo $title; ?>』<?php echo $teacherName; ?>">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">

                            <iframe class="iframe" src="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4"><?php echo $teacherName; ?></h3>
                            <p class="txt mb-1">株式会社ベクトル　海外事業本部</p>
                            <p class="txt">
                                海外にてMBA取得後、外資系企業にてグローバルマーケティング及びPRを従事。株式会社ベクトルでは、台湾・香港・韓国拠点にて幅広いクライアント事業を担当。特にインバウンド関連事業では、SNS運営を始めとしたマーケティング施策の責任者として従事。地方自治体を始め、鉄道・航空関連等の様々な業務経験がある。現在はベクトル本社海外事業本部のマネージャーとして在籍。
                            </p>
                        </div>

                        <!-- <div class="group_btn">
                            <a href="mailto:info@inbound-seminar.jp?subject=%E3%80%902020 %E7%8F%BE%E5%A0%B4%E5%AF%BE%E5%BF%9C%E8%80%85%E5%90%91%E3%81%91%E3%82%AA%E3%83%B3%E3%83%A9%E3%82%A4%E3%83%B3%E8%AC%9B%E8%A9%B1%E3%82%B5%E3%82%A4%E3%83%88%E3%80%91%E5%8B%95%E7%94%BB%E8%B3%AA%E5%95%8F%E3%83%A1%E3%83%BC%E3%83%AB&body=%E3%80%90%E3%81%8A%E5%90%8D%E5%89%8D%E3%80%91%0D%0A%0D%0A%E3%80%90%E6%89%80%E5%B1%9E%E3%80%91%E4%BE%8B%EF%BC%89%E3%80%87%E3%80%87%E7%94%BA%E8%A6%B3%E5%85%89%E5%8D%94%E4%BC%9A+%E7%AD%89%0D%0A%0D%0A%E3%80%90%E3%83%A1%E3%83%BC%E3%83%AB%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%80%91%0D%0A%0D%0A%E3%80%90%E5%8B%95%E7%94%BB%E5%90%8D%E3%80%91%E3%80%8Cwith%E3%82%B3%E3%83%AD%E3%83%8A%E6%99%82%E4%BB%A3%E3%81%AE%E3%82%A4%E3%83%B3%E3%83%90%E3%82%A6%E3%83%B3%E3%83%89%E8%A6%B3%E5%85%89%E3%80%8D%E5%8E%9F%E5%BF%A0%E4%B9%8B%0D%0A%0D%0A%E3%80%90%E8%B3%AA%E5%95%8F%E4%BA%8B%E9%A0%85%E3%80%91" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</a>
                        </div> -->
						<!-- <p class="caption text-center">
							この動画に質問がある方は、以下のアドレス宛にメールを送信してください。<br><br>
							「 info@inbound-seminar.jp 」
						</p> -->

                        <div class="group_btn">
                        <button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
                        </div>
                        <!--<p class="caption text-center">※この動画への質問期間は<span class="txt_red">12月10日</span>までとなります。</p>-->
                    </div>
                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
