<?php

$cat            = "分野別講話";//カテゴリー
$title          = "ハラル・ベジタリアンの食事対応について";//動画タイトル
$teacherName    = "大山 幸彦";//講師
 ?>

<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title><?php echo $title; ?> | <?php echo $cat; ?> | 2021 現場対応者向けオンライン講話サイト</title>
</head>



<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/header_2021.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2"><?php echo $cat; ?><br />［ホスピタリティ］</h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
            <div class="inner">
                <ol class="c_breadcrumb_list">
                    <li class="c_breadcrumb_item"><a href="/2021">TOP</a> ＞ </li>
                    <li class="c_breadcrumb_item"><a href="/2021/index.php#bunya"><?php echo $cat; ?></a> ＞</li>
                    <li class="c_breadcrumb_item">『<?php echo $title; ?>』<?php echo $teacherName; ?></li>
                </ol>
            </div>
        </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3"><?php echo $title; ?></h2>
                        <input type="hidden" name="video_name" value="『<?php echo $title; ?>』<?php echo $teacherName; ?>">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">

                            <iframe class="iframe" src="https://www.youtube.com/embed/IKvEBPBh1BU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4"><?php echo $teacherName; ?></h3>
                            <p class="txt mb-1">
                            1966年小樽生まれ函館育ち。編集者や楽器演奏者として東京と一時期パリに滞在した後、航空会社や映像会社での海外関連業務、クルーズ船や芸術祭、空港業務の英語仏語スタッフとして活動し、2018年より全国通訳案内士（英語）として北海道や本州のガイド活動に入る。2020年度には観光庁の定める「観光人材に対するインバウンド研修1級講師」に任命され活動し、同じく観光庁の「DMO外部専門人材」に登用される。実際のガイド活動のほか、日本遺産「炭鉄港」、北海道通訳案内士協会、商工会議所の札幌創成街歩きガイド、などでガイド研修の講師を担当する。​
                            </p>
															<!-- <pre class="txt">資格
2018年 全国通訳案内士合格（英） 北海道知事登録EN00343号
2016年 札幌特区通訳案内士合格（英） 札幌市長登録 EN00070号　
国内旅程管理業務主任者資格修得
北海道フードマイスター
おたる案内人一級

過去一年間の主な仕事
★FIT/ クルーズ のガイド 多数
★募集型企画旅行のガイド 多数
★インセンティブ、FAMトリップ（札幌、函館、小樽）
★商工会議所ローカルガイドツアーの講師
★まち作り会社主催ツアーの企画
													</pre> -->
                        </div>

                        <!-- <div class="group_btn">
                            <a href="mailto:info@inbound-seminar.jp?subject=%E3%80%902020 %E7%8F%BE%E5%A0%B4%E5%AF%BE%E5%BF%9C%E8%80%85%E5%90%91%E3%81%91%E3%82%AA%E3%83%B3%E3%83%A9%E3%82%A4%E3%83%B3%E8%AC%9B%E8%A9%B1%E3%82%B5%E3%82%A4%E3%83%88%E3%80%91%E5%8B%95%E7%94%BB%E8%B3%AA%E5%95%8F%E3%83%A1%E3%83%BC%E3%83%AB&body=%E3%80%90%E3%81%8A%E5%90%8D%E5%89%8D%E3%80%91%0D%0A%0D%0A%E3%80%90%E6%89%80%E5%B1%9E%E3%80%91%E4%BE%8B%EF%BC%89%E3%80%87%E3%80%87%E7%94%BA%E8%A6%B3%E5%85%89%E5%8D%94%E4%BC%9A+%E7%AD%89%0D%0A%0D%0A%E3%80%90%E3%83%A1%E3%83%BC%E3%83%AB%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%80%91%0D%0A%0D%0A%E3%80%90%E5%8B%95%E7%94%BB%E5%90%8D%E3%80%91%E3%80%8Cwith%E3%82%B3%E3%83%AD%E3%83%8A%E6%99%82%E4%BB%A3%E3%81%AE%E3%82%A4%E3%83%B3%E3%83%90%E3%82%A6%E3%83%B3%E3%83%89%E8%A6%B3%E5%85%89%E3%80%8D%E5%8E%9F%E5%BF%A0%E4%B9%8B%0D%0A%0D%0A%E3%80%90%E8%B3%AA%E5%95%8F%E4%BA%8B%E9%A0%85%E3%80%91" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</a>
                        </div> -->
						<!-- <p class="caption text-center">
							この動画に質問がある方は、以下のアドレス宛にメールを送信してください。<br><br>
							「 info@inbound-seminar.jp 」
						</p> -->

                        <div class="group_btn">
                        <button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
                        </div>
                        <!--<p class="caption text-center">※この動画への質問期間は<span class="txt_red">12月10日</span>までとなります。</p>-->
                    </div>
                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
