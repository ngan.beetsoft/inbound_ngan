<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
	<!-- OGPを使用することを宣言するタグ -->
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッド共通パーツ読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
	?>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
	<meta property="og:url" content="https://inbound-seminar.jp"><!-- OGPを設定するWEBページのURLを指定します。 -->
	<meta property="og:title" content="2021 現場対応者向けオンライン講話サイト"><!-- ページのtitleを指定します。 -->
	<meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
	<meta property="og:site_name" content="2021 現場対応者向けオンライン講話サイト"><!-- ページのサイト名を記述します。 -->
	<meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
	<meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

	<title>2021 現場対応者向けオンライン講話サイト</title>
</head>

<body>
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッダー読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/header_2021.php");
	?>

	<main>
		<section class="banner">
			<h1 class="ttl_1">2021 現場対応者向け<br>オンライン講話</h1>
			<img src="/common/images/banner.png" class="pc" alt="dummy">
			<img src="/common/images/banner_sp.png" class="sp" alt="dummy">
		</section>
		<div class="content">
			<div class="inner">
				<div class="ttl bg-5" id="kicho">
					<h2 class="ttl_3">基調講話</h2>
				</div>
				<div class="card">
					<ul class="list_card">
						<li>
							<a href="/2021/kicho/hara">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_hara.jpg" alt="原 忠之">
								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/10/6</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約43分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">原 忠之</h3>
									<p class="txt">セントラルフロリダ大学<br>ローゼン・ホスピタリティ経営学部テニュア付准教授、兼デイックポープ観光研究所上級研究員</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/kicho/abe_1">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_abe.jpg" alt="阿部 晃士">

								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/25</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約34分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">阿部 晃士</h3>
									<p class="txt">JTB Australia Pty Ltd. 取締役社長</p>
								</div>
							</a>
						</li>

						<li>
							<a href="/2021/kicho/abe_2">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_abe.jpg" alt="阿部 晃士">

								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/25</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約34分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">阿部 晃士</h3>
									<p class="txt">JTB Australia Pty Ltd. 取締役社長</p>
								</div>
							</a>
						</li>

						<li>
							<a href="/2021/kicho/joy">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_joy.jpg" alt="黄 玉雲（JOY HUANG）">

								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/9</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約25分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">黄 玉雲（JOY HUANG）</h3>
									<p class="txt">日盟国際商務有限公司　代表取締役社長</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/kicho/imu">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_imu.jpg" alt="任 栄鴻（イム　ヨンホン）">
								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/10/1</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約35分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">任 栄鴻（イム　ヨンホン）</h3>
									<p class="txt">株式会社　MEC　代表理事</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/kicho/aira_1">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_aira.jpg" alt="アイラ・フランコ">

								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/25</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約20分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">アイラ・フランコ</h3>
									<p class="txt">Inditex Zara Stockholm, Hamngatan<br>Sales Assistant</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/kicho/aira_2">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_aira.jpg" alt="アイラ・フランコ">

								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/25</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約20分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">アイラ・フランコ</h3>
									<p class="txt">Inditex Zara Stockholm, Hamngatan<br>Sales Assistant</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/kicho/chitose">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_chitose.jpg" alt="千歳 香奈子">

								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">千歳 香奈子</h3>
									<p class="txt">米国LA 南カリフォルニア道産子会　会長</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/kicho/yokoyama">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_yokoyama.jpg" alt="横山 明美">

								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/10/9</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">横山 明美</h3>
									<p class="txt">英国ロンドンで和食文化を広める和食講師、シェフ、コンサルタント、プレゼンター。</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/kicho/sugae">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_sugae.jpg" alt="菅江 崇">

								</figure>
								<div class="body_card">
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/28</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約18分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">菅江 崇</h3>
									<p class="txt">ジーリーメディア　役員</p>
								</div>
							</a>
						</li>
					</ul>
				</div>
				<div class="ttl bg-5" id="bunya">
					<h2 class="ttl_3">分野別講話</h2>
				</div>
				<div class="card">
					<ul class="list_card">
						<li>
							<a href="/2021/bunya/ooyama/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_ooyama.jpg" alt="大山 幸彦">

								</figure>
								<div class="body_card">
									<div class="category cate_1">ホスピタリティ（接遇）</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/21</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約26分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">大山 幸彦</h3>
									<p class="txt">全国通訳案内士</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/higaya/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_higaya.jpg" alt="飛ヶ谷 園子">

								</figure>
								<div class="body_card">
									<div class="category cate_1">ホスピタリティ（接遇）</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/16</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約20分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">飛ヶ谷 園子</h3>
									<p class="txt">全国通訳案内士</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/kusumi/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_kusumi.jpg" alt="久住 奈水子">

								</figure>
								<div class="body_card">
									<div class="category cate_1">ホスピタリティ（接遇）</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/24</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約18分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">久住 奈水子</h3>
									<p class="txt">全国通訳案内士</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/endou/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_endou.jpg" alt="遠藤 昌子">

								</figure>
								<div class="body_card">
									<div class="category cate_1">ホスピタリティ（接遇）</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">遠藤 昌子</h3>
									<p class="txt">合同会社　H-SEG（エイチーセグ）代表<br>国家資格通訳案内士、札幌大学非常勤講師</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/tajiri/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_tajiri.jpg" alt="田尻 〇〇">

								</figure>
								<div class="body_card">
									<div class="category cate_2">新市場</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">田尻 〇〇</h3>
									<p class="txt">JNTO日本政府観光局マレーシア</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/nguyen/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_nguyen.jpg" alt="グェン・ホアン・リン ">

								</figure>
								<div class="body_card">
									<div class="category cate_2">新市場</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/10/12</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">グェン・ホアン・リン </h3>
									<p class="txt">株式会社FPTテクノジャパン　代表取締役<br>アジア最大のIT企業グループ（ベトナム）</p>
								</div>
							</a>
						</li>


						<li>
							<a href="/2021/bunya/komori_1/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_komori.jpg" alt="小森 彩花">

								</figure>
								<div class="body_card">
									<div class="category cate_5">SNS</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/10/6</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">小森 彩花</h3>
									<p class="txt">株式会社ベクトル　海外事業本部</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/komori_2/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_komori.jpg" alt="小森 彩花">

								</figure>
								<div class="body_card">
									<div class="category cate_5">SNS</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/10/6</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">小森 彩花</h3>
									<p class="txt">株式会社ベクトル　海外事業本部</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/yagi/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_yagi.jpg" alt="八木 雄志">

								</figure>
								<div class="body_card">
									<div class="category cate_5">SNS</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/10/5</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約39分</dd>
										</dl>
									</div>
									<h3 class="ttl_4">八木 雄志</h3>
									<p class="txt">上海凸版利豊広告　マーケ・広告本部</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/shibayama/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_shibayama.jpg" alt="柴山 明寛">

								</figure>
								<div class="body_card">
									<div class="category cate_4">災害・感染症</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">柴山 明寛</h3>
									<p class="txt">東北大学災害科学国際研究所情報管理・社会連携部門 災害アーカイブ研究分野 准教授。博士（工学）</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya/kitama/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_kitama.jpg" alt="北間 砂織">

								</figure>
								<div class="body_card">
									<div class="category cate_4">災害・感染症</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/10/8</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">北間 砂織</h3>
									<p class="txt">北海道大学・札幌医科大学・藤女子大学・東海大学非常勤講師 会議(同時)通訳者 NPO法人SEMIさっぽろ副代表理事 国際臨床医学会認定 医療通訳士</p>
								</div>
							</a>
						</li>
						<!-- <li>
							<a href="/2021/bunya//">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_.jpg" alt="◯◯◯◯◯◯◯◯◯◯◯">

								</figure>
								<div class="body_card">
									<div class="category cate_3">SDGs</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">◯◯◯◯◯◯◯◯◯◯◯</h3>
									<p class="txt">岐阜県観光誘客推進課</p>
								</div>
							</a>
						</li> -->
						<!-- <li>
							<a href="/2021/bunya/paul/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_paul.jpg" alt="ポール・ハガード">

								</figure>
								<div class="body_card">
									<div class="category cate_3">SDGs</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">ポール・ハガード</h3>
									<p class="txt">インバウンドコンサルタント</p>
								</div>
							</a>
						</li> -->
						<li>
							<a href="/2021/bunya/arai/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_arai.jpg" alt="荒井 一洋">

								</figure>
								<div class="body_card">
									<div class="category cate_3">SDGs</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">荒井 一洋</h3>
									<p class="txt">NPO法人 大雪山自然学校　代表理事<br>北海道アドベンチャートラベル協議会　会長<br>NPO法人 日本エコツーリズムセンター　理事</p>
								</div>
							</a>
						</li>


					</ul>

				</div>
				<div class="ttl bg-5" id="kokunai">
					<h2 class="ttl_3">国内事例</h2>
				</div>
				<div class="card">
					<ul class="list_card">
						<li>
							<a href="/2021/kokunai/chiba/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_chiba.jpg" alt="地場 裕理子・蒲原 士郎">

								</figure>
								<div class="body_card">
									<div class="category cate_6">道外事例SNS</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">地場 裕理子・蒲原 士郎</h3>
									<p class="txt">公益財団法人高知県観光コンベンション協会</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/kokunai/honda/">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_honda.jpg" alt="本田 寛康">

								</figure>
								<div class="body_card">
									<div class="category cate_7">道内事例WeChat</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/9/28</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約15分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">本田 寛康</h3>
									<p class="txt">富良野市　経済部 商工観光課長</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2021/bunya//">
								<figure class="thumbnail">
									<img src="/common/images/2021/top/thumb_.jpg" alt="◯◯◯◯◯◯◯◯◯◯◯">

								</figure>
								<div class="body_card">
									<div class="category cate_3">SDGs</div>
									<div class="movie_info_warp">
										<dl class="movie_info">
											<dt>収録日</dt>
											<dd>2021/00/00</dd>
										</dl>
										<dl class="movie_info">
											<dt>動画時間</dt>
											<dd>約00分</dd>
										</dl>
									</div>

									<h3 class="ttl_4">◯◯◯◯◯◯◯◯◯◯◯</h3>
									<p class="txt">岐阜県観光誘客推進課</p>
								</div>
							</a>
						</li>



					</ul>
				</div>
			</div>
		</div>
	</main>

	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// フッター読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
	?>
</body>

</html>
