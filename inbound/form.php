<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
	<!-- OGPを使用することを宣言するタグ -->
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッド共通パーツ読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
	?>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
	<meta property="og:url" content="https://inbound-seminar.jp"><!-- OGPを設定するWEBページのURLを指定します。 -->
	<meta property="og:title" content="2020 現場対応者向けオンライン講話サイト"><!-- ページのtitleを指定します。 -->
	<meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
	<meta property="og:site_name" content="2020 現場対応者向けオンライン講話サイト"><!-- ページのサイト名を記述します。 -->
	<meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
	<meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

	<title>2020 現場対応者向けオンライン講話サイト</title>
</head>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> -->

<body>

	<main>

		<iframe id="frame" src="https://docs.google.com/forms/d/e/1FAIpQLSfnewJ25WOWUmrUs0yYeDBsRDisMhzt7LqCR2grcgVvacJphA/viewform?embedded=true" width="640" height="6508" frameborder="0" marginheight="0" marginwidth="0">読み込んでいます…</iframe>

	</main>

	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// フッター読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
	?>
</body>


<script>
$(function(){

    $('#frame').on('load', function(){
        $('#frame').contents().find('head').append('<link rel="stylesheet" href="/common/css/form.css">');
    });
});
</script>

</html>
