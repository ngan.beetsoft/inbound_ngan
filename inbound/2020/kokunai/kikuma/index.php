<?php
session_start(); //session開始
session_destroy();
?>
<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>自然や文化を通して地域の魅力をつなぐ　菊間 彰 | 国内事例 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2">国内事例</h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
            <div class="inner">
                <ol class="c_breadcrumb_list">
                    <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
                    <li class="c_breadcrumb_item"><a href="/2020/index.php#kokunai">国内事例</a> ＞</li>
                    <li class="c_breadcrumb_item">『自然や文化を通して地域の魅力をつなぐ』菊間 彰</li>
                </ol>
            </div>
        </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3">自然や文化を通して地域の魅力をつなぐ</h2>
                        <input type="hidden" name="video_name" value="『自然や文化を通して地域の魅力をつなぐ』菊間 彰">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">
                            <iframe class="iframe" src="https://www.youtube.com/embed/2ytwUe_DGnk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4">菊間 彰</h3>
                            <p class="txt mb-1">一般社団法人をかしや 代表理事</p>
                            <p class="txt">1974年生まれ。「一般社団法人をかしや」代表理事。環境教育と自然ガイド（インタープリテーション）が専門。ロープワークやナイフワーク、火起こしなどアウトドアスキル全般も得意。20代はじめに猿岩石に影響されバックパッカーとして東南アジアを巡る。その後、富士山麓、沖繩、名古屋、新潟など全国で自然に関わる仕事をしたのち2008年より愛媛県今治市に移住「をかしや」を起業。自然体験のノウハウをベースとした、行政向け、企業向け、一般向け研修を多数実施。「まごころこめて、ほんものを提供する」がモットー。</p>
                        </div>
                        <div class="group_btn">
                        <button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
                        </div>
                        <!-- <p class="caption text-center">※この動画への質問期間は<span class="txt_red">◯月◯日</span>までとなります。</p> -->
                    </div>
                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
