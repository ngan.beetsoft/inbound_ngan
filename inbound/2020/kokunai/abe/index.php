<?php
session_start(); //session開始
session_destroy();
?>
<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>農泊発祥の地”安心院”の概要　安部 翼 | 国内事例 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2">国内事例</h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
            <div class="inner">
                <ol class="c_breadcrumb_list">
                    <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
                    <li class="c_breadcrumb_item"><a href="/2020/index.php#kokunai">国内事例</a> ＞</li>
                    <li class="c_breadcrumb_item">『農泊発祥の地”安心院”の概要』安部 翼</li>
                </ol>
            </div>
        </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3">農泊発祥の地”安心院”の概要</h2>
                        <input type="hidden" name="video_name" value="『農泊発祥の地”安心院”の概要』安部 翼">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">
                            <iframe class="iframe" src="https://www.youtube.com/embed/Wf-X8Q1dzjc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4">安部 翼</h3>
                            <p class="txt mb-1">NPO法人 安心院町グリーンツーリズム研究会　事務局長 </p>
                            <p class="txt">安心院町グリーンツーリズム研究会は1996年に発足し、「農村民泊」に全国で初めて取り組み、都市と農村が交流するグリーンツーリズムの先駆けとして注目を集めてきました。
											町内で開催される恒例イベントの「ワイン祭り」に来たお客様に、B&B（朝食とベッド）を提供し始めたことが、「会員制農泊」のはじまりとされます。
											この日本で初めてのグリーンツーリズムのプロジェクトの中で、インバウンドに関する事例を紹介いたします。</p>
                        </div>
                        <div class="group_btn">
                        <button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
                        </div>
                        <!-- <p class="caption text-center">※この動画への質問期間は<span class="txt_red">◯月◯日</span>までとなります。</p> -->
                    </div>
                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
