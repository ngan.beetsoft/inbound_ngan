<?php
session_start(); //session開始
session_destroy();
?>
<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>オオタカ・オジロワシウォッチングツアーにおけるインバウンド対応　中川 貢 | 国内事例 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2">国内事例</h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
            <div class="inner">
                <ol class="c_breadcrumb_list">
                    <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
                    <li class="c_breadcrumb_item"><a href="/2020/index.php#kokunai">国内事例</a> ＞</li>
                    <li class="c_breadcrumb_item">『オオタカ・オジロワシウォッチングツアーにおけるインバウンド対応』中川 貢</li>
                </ol>
            </div>
        </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3">オオタカ・オジロワシウォッチングツアーにおけるインバウンド対応</h2>
                        <input type="hidden" name="video_name" value="『オオタカ・オジロワシウォッチングツアーにおけるインバウンド対応』中川 貢">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">
                            <iframe class="iframe" src="https://www.youtube.com/embed/PPVwVcwkERA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4">中川 貢</h3>
                            <p class="txt mb-1">浦河観光協会 コーディネーター</p>
                            <p class="txt">
                            ■職歴（経歴）<br>
                            　平成28年4月～　　浦河観光協会　事業戦略部長<br>
                            ■主な活動内容<br>
                            　馬産地として有名な浦河町でツアープログラム造成やガイドをしています。<br>
                            ■活動に対する思いなど<br>
                            　海山川に囲まれた自然豊かな町で農業体験ツアーや馬に特化したツアー、オオワシ・オジロワシ観察プログラムなど着地型観光を行い、町の活性化に取り組んでいます。
                            </p>
                        </div>
                        <div class="group_btn">
                        <button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
                        </div>
                        <!-- <p class="caption text-center">※この動画への質問期間は<span class="txt_red">◯月◯日</span>までとなります。</p> -->
                    </div>
                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
