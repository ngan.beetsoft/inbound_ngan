<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>インバウンド研修:病気やケガへの対応　北間 砂織 | 分野別講話 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2">分野別講話<span class="ttl_2_small">［コロナ感染症］</span></h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
            <div class="inner">
                <ol class="c_breadcrumb_list">
                    <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
                    <li class="c_breadcrumb_item"><a href="/2020/index.php#bunya">分野別講話［コロナ感染症］</a> ＞</li>
                    <li class="c_breadcrumb_item">『インバウンド研修:病気やケガへの対応』北間 砂織</li>
                </ol>
            </div>
        </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3">インバウンド研修:病気やケガへの対応</h2>
                        <input type="hidden" name="video_name" value="『インバウンド研修:病気やケガへの対応』北間 砂織">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">
                            <iframe class="iframe" src="https://www.youtube.com/embed/9IDPbYclh1k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4">北間 砂織</h3>
                            <p class="txt mb-1">北海道大学・札幌医科大学・藤女子大学・東海大学非常勤講師 会議(同時)通訳者 NPO法人SEMIさっぽろ副代表理事 国際臨床医学会認定 医療通訳士</p>
                            <p class="txt">英国エセックス大学大学院修了。英語学・言語学修士。英語会議通訳者、国際臨床医学会認定医療通訳士。医療系の大学で英語の非常勤講師を担当する際に、医療と英語の接点を探し、医療通訳という存在を知る。2009年4月に有志とともに札幌英語医療通訳グループ（SEMI）を設立。2019年にNPO法人SEMIさっぽろに改組し、副代表理事を務める。在住外国人、観光客、先進医療を求めて医療ビザで来日する患者さんなどのために幅広く医療通訳を提供する。北海道大学、札幌医科大学、藤女子大学、東海大学非常勤講師。日本医学英語教育学会、日本通訳翻訳学会、国際臨床医学会会員。</div>


<?php //if($nowtime < 202012100000)://期限を過ぎたら非表示にする ?>
						<div class="group_btn">
							<button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
						</div>

						<?php if($nowtime > 202102000000)://期限を過ぎたら表示にする ?>
						<p class="caption text-center">※この動画への質問は12月10日までとさせていただきます。</p>
						<?php endif; ?>

<?php //endif; ?>


                    </div>

                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
