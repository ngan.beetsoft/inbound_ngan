<?php
session_start(); //session開始
session_destroy();
?>
<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>アジア新興市場の動向 〜台湾・マレーシア・フィリピン・タイ・ベトナム市場を中心に〜　柿島 あかね | 分野別講話 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2">分野別講話<span class="ttl_2_small">［新市場対応］</span></h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
		   <div class="inner">
			   <ol class="c_breadcrumb_list">
				   <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
				   <li class="c_breadcrumb_item"><a href="/2020/index.php#bunya">分野別講話［新市場対応］</a> ＞</li>
				   <li class="c_breadcrumb_item">『アジア新興市場の動向 〜台湾・マレーシア・フィリピン・タイ・ベトナム市場を中心に〜』柿島 あかね</li>
			   </ol>
		   </div>
	   </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3">アジア新興市場の動向 〜台湾・マレーシア・フィリピン・タイ・ベトナム市場を中心に〜</h2>
                        <input type="hidden" name="video_name" value="『アジア新興市場の動向 〜台湾・マレーシア・フィリピン・タイ・ベトナム市場を中心に〜』柿島 あかね">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">
                            <iframe class="iframe" src="https://www.youtube.com/embed/bgriSAbpG8s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4">柿島 あかね</h3>
                            <input type="hidden" name="title_name" value="柿島 あかね">
                            <p class="txt mb-1">公益財団法人　日本交通公社<br>観光経済研究部地域活性化室　主任研究員</p>
                            <p class="txt">

								2007年立教大学観光学研究科博士課程前期課程修了。同年（財）日本交通公社入社。<br>
								2016年より現職。琉球大学国際地域創造学部非常勤講師や神奈川県観光審議会、
								福岡県観光審議会の委員等を務める。専門はインバウンド市場に関する分析や観光に
								よる経済効果等。主な著書に「インバウンドの消費促進と地域経済活性化」（ぎょうせい、共著、2017年）、「旅行産業論」（（公財）日本交通公社、2016年）等がある。
                            </p>
                        </div>
                        <div class="group_btn">
                        <button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
                        </div>
                        <!--<p class="caption text-center">※この動画への質問期間は<span class="txt_red">12月11日</span>までとなります。</p>-->
                    </div>
                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
