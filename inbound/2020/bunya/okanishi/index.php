<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
	<!-- OGPを使用することを宣言するタグ -->
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッド共通パーツ読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
	?>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
	<meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
	<meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
	<meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
	<meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
	<meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
	<meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

	<title>外国人のお客様への対応　岡西 昭子 | 分野別講話 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッダー読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
	?>

	<main>
		<section class="banner">
			<h1 class="ttl_2">分野別講話<span class="ttl_2_small">［ホスピタリティ（接遇）］</span></h1>
			<img src="/common/images/banner.png" class="pc" alt="">
			<img src="/common/images/banner_sp.png" class="sp" alt="">
		</section>
		<nav class="c_breadcrumb">
		   <div class="inner">
			   <ol class="c_breadcrumb_list">
				   <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
				   <li class="c_breadcrumb_item"><a href="/2020/index.php#bunya">分野別講話［ホスピタリティ（接遇）］</a> ＞</li>
				   <li class="c_breadcrumb_item">『外国人のお客様への対応』岡西 昭子</li>
			   </ol>
		   </div>
	   </nav>
		<section class="content">
			<div class="inner">
				<form action="/form/" method="post">
					<div class="ttl bg-5">
						<h2 class="ttl_3">外国人のお客様への対応</h2>
						<input type="hidden" name="video_name" value="『外国人のお客様への対応』岡西 昭子">
					</div>
					<div class="detail_content">
						<div class="box_video">
							<iframe class="iframe" src="https://www.youtube.com/embed/V56CtF6TOnY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
						<div class="info">
							<h3 class="ttl_4">岡西 昭子</h3>
							<p class="txt mb-1">札幌グランドホテル コンシェルジュ・前日本コンシェルジュ協会会長</p>
							<p class="txt">ベルパーソンから、フロントを経て、コンシェルジュに就任。1997年コンシェルジュの世界的ネットワーク組織「レ・クレドール」に賛助会員として入会、2002年インターナショナルメンバーとして承認される。国内の同メンバーとともに、日本でのネットワークの拡充を図るため、日本コンシェルジュ協会の設立に携わり、08年から2019年4月まで会長を務める。満足の連鎖を続けるためにネットワークづくりを大切にしている。</p>
						</div>



<?php if($nowtime < 202012250000)://期限を過ぎたら非表示にする ?>
						<div class="group_btn">
							<button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
						</div>

						<?php if($nowtime > 202012180000)://期限を過ぎたら表示にする ?>
						<p class="caption text-center">※この動画への質問は12月25日までとさせていただきます。</p>
						<?php endif; ?>

<?php endif; ?>


					</div>

				</form>
			</div>
		</section>
	</main>

	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// フッター読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
	?>
</body>

</html>
