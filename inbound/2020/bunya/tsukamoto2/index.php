<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>観光地で、もし体調不良者が出てしまったら　塚本 容子 | 分野別講話 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2">分野別講話<span class="ttl_2_small">［コロナ感染症］</span></h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
            <div class="inner">
                <ol class="c_breadcrumb_list">
                    <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
                    <li class="c_breadcrumb_item"><a href="/2020/index.php#bunya">分野別講話［コロナ感染症］</a> ＞</li>
                    <li class="c_breadcrumb_item">『観光地で、もし体調不良者が出てしまったら』塚本 容子</li>
                </ol>
            </div>
        </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3">観光地で、もし体調不良者が出てしまったら</h2>
                        <input type="hidden" name="video_name" value="『観光地で、もし体調不良者が出てしまったら』塚本 容子">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">
                            <iframe class="iframe" src="https://www.youtube.com/embed/skfOGcglWYk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4">塚本 容子</h3>
                            <p class="txt mb-1">北海道医療大学 看護福祉学部 看護福祉学部研究科 臨床看護学講座 教授</p>
                            <p class="txt">1995 年~2005 年 米国にて CDC のインターンシップ後、ファミリーナースプラクティショナー の資格を取得カリフ ォルニア大学サンフランシスコメディカルセンター等でHIV 患者の診察 にあたる。スタンフォード大学博士課程にて 公衆衛生学博士を取得。
State Firm Insurance Company Doctoral Dissertation Award 受賞 ・ Orthobiotec Unrestricted Educational Grant: HIV 患者における Erythropoietin 治療に関する対費用 効果性に関する研究助成を受ける。講師、講演、研究、著書多数。</p>
                        </div>


<?php if($nowtime < 202012100000)://期限を過ぎたら非表示にする ?>
						<div class="group_btn">
							<button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
						</div>

						<?php if($nowtime > 202012030000)://期限を過ぎたら表示にする ?>
						<p class="caption text-center">※この動画への質問は12月10日までとさせていただきます。</p>
						<?php endif; ?>

<?php endif; ?>


                    </div>

                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
