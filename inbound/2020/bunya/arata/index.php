<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>アドベンチャートラベル・現場でのインバウンド対応　荒田 康仁 | 分野別講話 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
     include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2">分野別講話<span class="ttl_2_small">［アドベンチャートラベル］</span></h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
         <nav class="c_breadcrumb">
            <div class="inner">
                <ol class="c_breadcrumb_list">
                    <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
                    <li class="c_breadcrumb_item"><a href="/2020/index.php#bunya">分野別講話［アドベンチャートラベル］</a> ＞</li>
                    <li class="c_breadcrumb_item">『アドベンチャートラベル・現場でのインバウンド対応』荒田 康仁</li>
                </ol>
            </div>
        </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3">アドベンチャートラベル・現場でのインバウンド対応</h2>
                        <input type="hidden" name="video_name" value="『アドベンチャートラベル・現場でのインバウンド対応』荒田 康仁">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">
                            <iframe class="iframe" src="https://www.youtube.com/embed/rxBkfQk8GU8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4">荒田 康仁</h3>
                            <p class="txt mb-1">TREE LIFE　代表　北海道山岳協会認定ガイド</p>
                            <p class="txt">年間活動日数180日を超えて自然の中で楽しむ。登山歴30年、野山遊び歴40年。スキー場やその規制区域外、山岳地のバックカントリーまで様々な滑走でのツアーを実施。参加者に合わせ、スキー、スノーボード、テレマーク、雪山登山も対応。</p>
                        </div>


						<div class="group_btn">
							<button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
						</div>

                    </div>

                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
