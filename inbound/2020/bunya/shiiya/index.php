<?php
session_start(); //session開始
session_destroy();
?>
<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
    <!-- OGPを使用することを宣言するタグ -->
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッド共通パーツ読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
    ?>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
    <meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
    <meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
    <meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
    <meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
    <meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
    <meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

    <title>フィリピンとフィリピン人　その歴史と国民性　椎谷 泰世 | 分野別講話 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // ヘッダー読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
    ?>

    <main>
        <section class="banner">
            <h1 class="ttl_2">分野別講話<span class="ttl_2_small">［新市場対応］</span></h1>
			<img src="/common/images/banner.png" class="pc" alt="">
            <img src="/common/images/banner_sp.png" class="sp" alt="">
        </section>
        <nav class="c_breadcrumb">
		   <div class="inner">
			   <ol class="c_breadcrumb_list">
				   <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
				   <li class="c_breadcrumb_item"><a href="/2020/index.php#bunya">分野別講話［新市場対応］</a> ＞</li>
				   <li class="c_breadcrumb_item">『フィリピンとフィリピン人　その歴史と国民性』椎谷 泰世</li>
			   </ol>
		   </div>
	   </nav>
        <section class="content">
            <div class="inner">
                <form action="/form/" method="post">
                    <div class="ttl bg-5">
                        <h2 class="ttl_3">フィリピンとフィリピン人　その歴史と国民性</h2>
                        <input type="hidden" name="video_name" value="『フィリピンとフィリピン人　その歴史と国民性』椎谷 泰世">
                    </div>
                    <div class="detail_content">
                        <div class="box_video">
                            <iframe class="iframe" src="https://www.youtube.com/embed/jd8jr_3_0-w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="info">
                            <h3 class="ttl_4">椎谷 泰世</h3>
                            <input type="hidden" name="title_name" value="椎谷 泰世">
                            <p class="txt mb-1">全国通訳案内士 英語通訳ガイド</p>
                            <p class="txt">50年前、アマチュア無線を通じての交流をきっかけにフィリピンが身近に。2013年からは通訳ガイドとしてこの国の観光客を案内。これまでに28グループ431名を案内した。この経験を通じて陽気で信心深いフィリピン人に好感を持つようになる。今年2月下旬、道内旅行で親しくなったマニラの医師が、国に帰ってまもなくコロナに感染して急逝。ショックを受ける。世界を覆うコロナ感染拡大が1日も早く終息し、フィリピンからの観光客がやってくるのを心待ちにしている。
                            </p>
                        </div>

<?php //if($nowtime < 202000000000)://期限を過ぎたら非表示にする ?>
						<div class="group_btn">
							<button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
						</div>

						<?php if($nowtime > 202101000000)://期限を過ぎたら表示にする ?>
						<p class="caption text-center">※この動画への質問は12月10日までとさせていただきます。</p>
						<?php endif; ?>

<?php //endif; ?>
                    </div>
                </form>
            </div>
        </section>
    </main>

    <?php
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    // フッター読み込み
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
    ?>
</body>

</html>
