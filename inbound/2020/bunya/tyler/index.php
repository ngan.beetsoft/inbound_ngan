<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
	<!-- OGPを使用することを宣言するタグ -->
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッド共通パーツ読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
	?>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
	<meta property="og:url" content=""><!-- OGPを設定するWEBページのURLを指定します。 -->
	<meta property="og:title" content=""><!-- ページのtitleを指定します。 -->
	<meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
	<meta property="og:site_name" content=""><!-- ページのサイト名を記述します。 -->
	<meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
	<meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

	<title>インバウンド観光客へのおもてなし　タイラー リンチ | 分野別講話 | 2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッダー読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");
	?>

	<main>
		<section class="banner">
			<h1 class="ttl_2">分野別講話<span class="ttl_2_small">［ホスピタリティ（接遇）］</span></h1>
			<img src="/common/images/banner.png" class="pc" alt="">
			<img src="/common/images/banner_sp.png" class="sp" alt="">
		</section>
		<nav class="c_breadcrumb">
		   <div class="inner">
			   <ol class="c_breadcrumb_list">
				   <li class="c_breadcrumb_item"><a href="/2020">TOP</a> ＞ </li>
				   <li class="c_breadcrumb_item"><a href="/2020/index.php#bunya">分野別講話［ホスピタリティ（接遇）］</a> ＞</li>
				   <li class="c_breadcrumb_item">『インバウンド観光客へのおもてなし』タイラー リンチ</li>
			   </ol>
		   </div>
	   </nav>
		<section class="content">
			<div class="inner">
				<form action="/form/" method="post">
					<div class="ttl bg-5">
						<h2 class="ttl_3">インバウンド観光客へのおもてなし</h2>
						<input type="hidden" name="video_name" value="『インバウンド観光客へのおもてなし』タイラー リンチ">
					</div>
					<div class="detail_content">
						<div class="box_video">
							<iframe class="iframe" src="https://www.youtube.com/embed/yyLO449eLaE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
						<div class="info">
							<h3 class="ttl_4">タイラー リンチ</h3>
							<p class="txt mb-1">信州戸倉上山田温泉 亀清旅館 若旦那</p>
							<p class="txt">米国の西海岸のシアトル市出身、戌年。地元のワシントン大学卒、国際学部専門(日本語の 勉強も、神戸での留学も)大学を卒業してから上田市で1年半英会話講師として勤務。その 時に亀清の娘と知り合い、一緒にシアトルへ移住し、結婚。11年間、貿易関係の会社に勤務しながら生活。妻の実家、亀清の女将が高齢となり後継ぎ問題が発生。2005年10月に家族 を連れて日本へ移り、若旦那として後継ぎに。第二回信州おもてなし大賞受賞。長野県のインバウンド発展を目指しているNPO NINJAプロジェクトの代表</p>
						</div>



<?php if($nowtime < 202011190000)://期限を過ぎたら非表示にする ?>
						<div class="group_btn">
							<button type="submit" class="btn btn_1 mw-550">ご質問・ご感想フォームはこちら</button>
						</div>

						<?php if($nowtime > 202011120000)://期限を過ぎたら表示にする ?>
						<p class="caption text-center">※この動画への質問は11月19日までとさせていただきます。</p>
						<?php endif; ?>

<?php endif; ?>


					</div>

				</form>
			</div>
		</section>
	</main>

	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// フッター読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
	?>
</body>

</html>
