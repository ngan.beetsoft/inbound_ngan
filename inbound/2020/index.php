<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
	<!-- OGPを使用することを宣言するタグ -->
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッド共通パーツ読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/head.php");
	?>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<meta property="og:type" content="website"><!-- ページの種類を指定します。 -->
	<meta property="og:url" content="https://inbound-seminar.jp"><!-- OGPを設定するWEBページのURLを指定します。 -->
	<meta property="og:title" content="2020 現場対応者向けオンライン講話サイト"><!-- ページのtitleを指定します。 -->
	<meta property="og:description" content=""><!-- ページの説明文を指定します。 -->
	<meta property="og:site_name" content="2020 現場対応者向けオンライン講話サイト"><!-- ページのサイト名を記述します。 -->
	<meta property="og:image" content="https://<?php echo $_SERVER['HTTP_HOST']; ?>/common/images/ogp.jpg"><!-- SNS上でシェアされた際に表示させたい画像を絶対パスで指定します。 -->
	<meta name="twitter:card" content="summary_large_image"><!-- Twitter上での表示タイプを指定するタグ -->

	<title>2020 現場対応者向けオンライン講話サイト</title>
</head>

<body>
	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ヘッダー読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/header.php");

	?>

	<main>
		<section class="banner">
			<h1 class="ttl_1">2020 現場対応者向け<br>オンライン講話</h1>
			<img src="/common/images/2020/top/banner.png" class="pc" alt="dummy">
			<img src="/common/images/2020/top/banner_sp.png" class="sp" alt="dummy">
		</section>
		<div class="content">
			<div class="inner">
				<div class="ttl bg-5" id="kicho">
					<h2 class="ttl_3">基調講話</h2>
				</div>
				<div class="card">
					<ul class="list_card">
						<li>
							<a href="/2020/kicho/hara">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_hara.jpg" alt="原 忠之">
								</figure>
								<div class="body_card">
									<h3 class="ttl_4">原 忠之</h3>
									<p class="txt">セントラルフロリダ大学<br>ローゼン・ホスピタリティ経営学部テニュア付准教授、兼デイックポープ観光研究所上級研究員</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/kicho/kimoto">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_kimoto.jpg" alt="木本 晃">

								</figure>
								<div class="body_card">
									<h3 class="ttl_4">木本 晃</h3>
									<p class="txt">一般社団法人 北海道観光を考えるみんなの会会長</p>
								</div>
							</a>
						</li>
					</ul>
				</div>
				<div class="ttl bg-5" id="bunya">
					<h2 class="ttl_3">分野別講話</h2>
				</div>
				<div class="card">
					<ul class="list_card">
						<li>
							<a href="/2020/bunya/tyler/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_tyler.jpg" alt="タイラー リンチ">

								</figure>
								<div class="body_card">
									<div class="category cate_1">ホスピタリティ（接遇）</div>
									<h3 class="ttl_4">タイラー リンチ</h3>
									<p class="txt">信州戸倉上山田温泉 亀清旅館 若旦那</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/bunya/okanishi/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_okanishi.jpg" alt="岡西 昭子">

								</figure>
								<div class="body_card">
									<div class="category cate_1">ホスピタリティ（接遇）</div>
									<h3 class="ttl_4">岡西 昭子</h3>
									<p class="txt">札幌グランドホテル コンシェルジュ ・ 前日本コンシェルジュ協会会長</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/bunya/kakishima/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_kakisima.jpg" alt="柿島 あかね">

								</figure>
								<div class="body_card">
									<div class="category cate_2">新市場対応</div>
									<h3 class="ttl_4">柿島 あかね</h3>
									<p class="txt">公益財団法人　日本交通公社<br>観光経済研究部地域活性化室　主任研究員</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/bunya/shiiya/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_shiiya.jpg" alt="椎谷 泰世">

								</figure>
								<div class="body_card">
									<div class="category cate_2">新市場対応</div>
									<h3 class="ttl_4">椎谷 泰世</h3>
									<p class="txt">全国通訳案内士 英語通訳ガイド</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/bunya/saga/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_saga.jpg" alt="佐賀 彩美">

								</figure>
								<div class="body_card">
									<div class="category cate_3">アドベンチャートラベル</div>
									<h3 class="ttl_4">佐賀 彩美</h3>
									<p class="txt">一般社団法人北海道開発技術センター 調査研究部 研究員</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/bunya/arata/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_arata.jpg" alt="荒田 康仁">

								</figure>
								<div class="body_card">
									<div class="category cate_3">アドベンチャートラベル</div>
									<h3 class="ttl_4">荒田 康仁</h3>
									<p class="txt">TREE LIFE　代表　北海道山岳協会認定ガイド</p>
								</div>
							</a>
						</li>

						<li>
							<a href="/2020/bunya/tsukamoto/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_tsukamoto.jpg" alt="塚本 容子">

								</figure>
								<div class="body_card">
									<div class="category cate_4">コロナ感染症</div>
									<h3 class="ttl_4">塚本 容子</h3>
									<p class="txt">北海道医療大学 看護福祉学部 看護福祉学部研究科 臨床看護学講座 教授</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/bunya/tsukamoto2/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_tsukamoto_2.jpg" alt="塚本 容子">

								</figure>
								<div class="body_card">
									<div class="category cate_4">コロナ感染症</div>
									<h3 class="ttl_4">塚本 容子</h3>
									<p class="txt">北海道医療大学 看護福祉学部 看護福祉学部研究科 臨床看護学講座 教授</p>
								</div>
							</a>
						</li>

						<li>
							<a href="/2020/bunya/kitama/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_kitama.jpg" alt="北間 砂織">

								</figure>
								<div class="body_card">
									<div class="category cate_4">コロナ感染症</div>
									<h3 class="ttl_4">北間 砂織</h3>
									<p class="txt">北海道大学・札幌医科大学・藤女子大学・東海大学非常勤講師 会議(同時)通訳者 NPO法人SEMIさっぽろ副代表理事 国際臨床医学会認定 医療通訳士</p>
								</div>
							</a>
						</li>
						<!-- <li>
							<a href="#">
								<figure class="thumbnail">
									<img src="/common/images/top/thumb_iwasaki.jpg" alt="岩崎 修子">

								</figure>
								<div class="body_card">
									<div class="category cate_1">現場対応者向け（接遇）</div>
									<h3 class="ttl_4">岩崎 修子</h3>
									<p class="txt">全国通訳案内士、会議通訳(同時通訳)者、北海道通訳アカデミー講師、札幌医科大学非常勤講師</p>
								</div>
							</a>
						</li> -->

					</ul>
				</div>
				<div class="ttl bg-5" id="kokunai">
					<h2 class="ttl_3">国内事例</h2>
				</div>
				<div class="card">
					<ul class="list_card">
						<!-- <li>
							<a href="#">
								<figure class="thumbnail">
									<img src="/common/images/top/thumb_ogawa.jpg" alt="小川正人">

								</figure>
								<div class="body_card">
									<h3 class="ttl_4">小川正人</h3>
									<p class="txt">ガストロノミーツーリズム推進機構 専務理事</p>
								</div>
							</a>
						</li> -->
						<li>
							<a href="/2020/kokunai/nakagawa/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_nakagawa.jpg" alt="中川 貢">

								</figure>
								<div class="body_card">
									<h3 class="ttl_4">中川 貢</h3>
									<p class="txt">浦河観光協会 コーディネーター</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/kokunai/kikuma/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_kikuma.jpg" alt="菊間彰">

								</figure>
								<div class="body_card">
									<h3 class="ttl_4">菊間彰</h3>
									<p class="txt">一般社団法人をかしや 代表理事</p>
								</div>
							</a>
						</li>
						<li>
							<a href="/2020/kokunai/abe/">
								<figure class="thumbnail">
									<img src="/common/images/2020/top/thumb_abe.jpg" alt="安部 翼">

								</figure>
								<div class="body_card">
									<h3 class="ttl_4">安部 翼</h3>
									<p class="txt">NPO法人 安心院町グリーンツーリズム研究会　事務局長 </p>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</main>

	<?php
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// フッター読み込み
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	include($_SERVER['DOCUMENT_ROOT'] . "/navi/footer.php");
	?>
</body>

</html>
